---
author: Vincent-Xavier Jumel
title: Expéditions himalayennes
---

# :mountain: Ascension de l'Himalaya

On dispose d'une base de données des ascensions de l'Himalaya depuis 1903 compilés par Elizabeth Hawley, une journaliste spécialisée basée à Kathmandou.

Les différentes tables sont représentées par le schéma suivant :

![Le schéma](himalayan-database-schema.png)

La base est organisée de la façon suivante :

* La table `peaks` contient un enregistrement par montagne de l'Himalaya.
* La table `exped` possède un enregistrement par expédition.
* La table `members` décrit chaque membre significatif des expéditions.
* La table `refer` décrit la littérature sur les expéditions.

Cette base est téléchargeble au format *sqlite* [ici](himalayan_database.sqlite)

???+

    ```shell
    wget https://www.himalayandatabase.com/downloads/Himalayan%20Database.zip -O download/Himalayan_Database.zip
    unzip download/Himalayan_Database.zip -d download/
    ```

    ```python
    #!/usr/bin/env python

    import sqlite3
    from dbfread import DBF


    def get_fields(table):
        """get the fields and sqlite types for a dbf table"""
        typemap = {
            "F": "FLOAT",
            "L": "BOOLEAN",
            "I": "INTEGER",
            "C": "TEXT",
            "N": "REAL",  # because it can be integer or float
            "M": "TEXT",
            "D": "DATE",
            "T": "DATETIME",
            "0": "INTEGER",
        }

        fields = {}
        for f in table.fields:
            fields[f.name] = typemap.get(f.type, "TEXT")
        return fields


    def create_table_statement(table_name, fields):
        defs = ", ".join(['"%s" %s' % (fname, ftype) for (fname, ftype) in fields.items()])
        sql = 'create table "%s" (%s)' % (table_name, defs)
        return sql


    def insert_table_statement(table_name, fields):
        refs = ", ".join([":" + f for f in fields.keys()])
        sql = 'insert into "%s" values (%s)' % (table_name, refs)
        return sql


    def copy_table(cursor, table):
        """Add a dbase table to an open sqlite database"""
        cursor.execute("drop table if exists %s" % table.name)
        fields = get_fields(table)

        sql = create_table_statement(table.name, fields)
        cursor.execute(sql)

        sql = insert_table_statement(table.name, fields)

        for rec in table:
            cursor.execute(sql, list(rec.values()))


    def main():
        output_file = "himalayan_database.sqlite"
        tables = ["exped", "members", "peaks", "refer"]
        conn = sqlite3.connect(output_file)
        cursor = conn.cursor()

        for table_name in tables:
            table_file = f"download/Himalayan Database/HIMDATA/{table_name}.DBF"
            dbf_table = DBF(
                table_file, lowernames=True, encoding=None, char_decode_errors="strict"
            )
            copy_table(cursor, dbf_table)

        conn.commit()


    if __name__ == "__main__":
        main()
    ```

    Voir [cette page qui décrit la conversion](https://www.peter-hoffmann.com/2021/convert-the-himalayan-database-to-sqlite.html)

<!-- -->

1. Combien de sommets sont présents dans cette base de données ?

    !!! note "Votre réponse"
        {{ sqlide titre="SQL" base="43_himalaya/himalayan_database.sqlite" espace="himalaya" }}

2. 
